"""
Utility script to clean member URLs
===================================

This Python script is intended to clean a list of Tumblr URLs belonging to
members of a Facebook group who maintain the list of URLs as a file on
Facebook.

Requirements: Python 2.7+; requests library

Input:
    - ideal input: a sequence of lines such that each line contains a URL
      starting with "http://" or "https://", and optionally, whitespace
      followed by a name associated with that URL, e.g.,
      http://example.com
      http://example.com Example Member
    - worst case: a single line such that each entry starts with "http://" or
      "https://", and optionally whitespace followed by a name associated with
      that URL, e.g.,
      http://example.comhttp://example.com Example Memberhttp://example.com
    (Input will be read from standard input.)

Output: a sequence of lines in the same format as the ideal input, but sorted
        by URL with duplicates and URLs resulting in HTTP 404 errors removed.
        The URLs will be in lowercase.
        (Output will be printed to standard output.)

Example usage:
    $ python clean_member_urls.py < member_urls_in.txt > member_urls_out.txt
"""

from __future__ import absolute_import, print_function, unicode_literals

import fileinput
import re
import requests
import sys

ENTRY_REGEX = re.compile(r'((http|https)://.*?)((http|https)://.*)',
                         re.IGNORECASE | re.UNICODE)
ENTRY_END_REGEX = re.compile(r'((http|https)://.*)$',
                             re.IGNORECASE | re.UNICODE)
MEMBER_REGEX = re.compile(r'(\S+)\s*(.*)', re.UNICODE)


def read_member_urls():
    """Returns a dictionary of member URLs obtained from standard input."""
    line = ' '.join(line.decode('utf8').strip() for line in fileinput.input())

    member_urls = {}

    while True:
        match = ENTRY_REGEX.match(line)
        if match:
            entry = match.group(1)
            line = match.group(3)
        else:
            match = ENTRY_END_REGEX.match(line)
            if match:
                entry = match.group(1)
            else:
                entry = ''
            line = ''

        if entry:
            match = MEMBER_REGEX.match(entry)
            if match:
                url = match.group(1).lower()
                name = match.group(2).strip()
                if name or (url not in member_urls):
                    member_urls[url] = name
        else:
            break

    return member_urls


def exclude_not_found_urls(member_urls):
    """
    Returns a dictionary of member urls excluding those resulting in HTTP 404
    errors.
    """
    found_member_urls = {}
    for url in member_urls:
        try:
            response = requests.get(url)
            if response.status_code != 404:
                found_member_urls[url] = member_urls[url]
        except requests.exceptions.RequestException as e:
            print(e, file=sys.stderr)
    return found_member_urls


def compile_sorted_member_url_list(member_urls):
    """Returns a list of member URL/name pairs, sorted by URL."""
    return sorted(
        [{'url': url, 'name': member_urls[url]} for url in member_urls],
        key=lambda item: item['url'],
    )


def print_member_url_list(member_url_list, safety_margin=5):
    """Prints member URL list to standard output."""
    url_max_length = max(len(member_url['url'])
                         for member_url in member_url_list)
    line_format = '{{url:{}}} {{name}}'.format(url_max_length + safety_margin)
    for member_url in member_url_list:
        print(line_format.format(**member_url).rstrip().encode('utf8'))


if __name__ == '__main__':
    member_urls = read_member_urls()
    member_urls = exclude_not_found_urls(member_urls)
    member_url_list = compile_sorted_member_url_list(member_urls)
    print_member_url_list(member_url_list)
